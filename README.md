![Discuz](http://git.oschina.net/uploads/images/2016/1222/210445_0d4ab69a_1157835.png)

 **15 年梦想，我们 17 前行，愿 Discuz! 能永远活在 Fans 们的指尖和心中** 

### **简介** 

Discuz! X 官方 Git/SVN (https://git.oschina.net/ComsenzDiscuz/DiscuzX) ，简体中文 UTF8 版本，其他版本请自行转码或者在 Discuz! 官方站下载安装包。

### **声明**
您可以 Fork 本站代码，但未经许可 **禁止** 在本产品的整体或任何部分基础上以发展任何派生版本、修改版本或第三方版本用于 **重新分发** 

### **相关网站**
 
- Discuz! 官方站：http://www.discuz.net
- Discuz! 应用中心：http://addon.discuz.com
- Discuz! 开放平台：http://open.discuz.net

### **感谢 Fans**

- DiscuzFans：https://git.oschina.net/sinlody/DiscuzFans
- DiscuzLite：https://git.oschina.net/3dming/DiscuzL